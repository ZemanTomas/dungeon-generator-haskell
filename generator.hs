import Data.Array
import Data.Graph
import Data.List
import Data.List.Split
import Data.List.Unique
import Data.Maybe
import System.Environment 
import System.Exit
import System.Random
import Debug.Trace

-- Room Type (Room tl br)
data Room = Room {
    pos :: Rectangle,
    meta :: String
} deriving (Eq)
data Rectangle = Rectangle {
    x1 :: Int,
    y1 :: Int,
    x2 :: Int,
    y2 :: Int
} deriving (Eq)
-- Cells Type
type Cells = Array (Int, Int) Char
-- Dungeon Type
data Dungeon = Dungeon {
    cells :: Cells,
    width :: Int,
    height :: Int,
    rooms :: [Room],
    generator :: StdGen
}
--- Show Dungeon
instance Show Dungeon where
    show (Dungeon c w h r g) = (splitN (intercalate "" (map (\x -> [x]) (elems c))) h) ++ (intercalate "\n" (map (\(x,y) -> (show x)++(show y)) (zip [0,1..] r)))
--- Show Room
instance Show Room where
    show (Room r m) = ": "++show r++" "++ m
--- Show Rectangle
instance Show Rectangle where
    show (Rectangle x1 y1 x2 y2) = "("++show y1++":"++show x1++" "++show y2++":"++show x2++")"
--- Ord Rectangle by left top corner
instance Ord Rectangle where
    (Rectangle x1 y1 _ _) <= (Rectangle x2 y2 _ _) = ((x1 == x2) && (y1 == y2)) || ((x1 < x2) || ((x1 == x2) && (y1 < y2)))
--- Ord Room by Rectangle
instance Ord Room where
    (Room p1 _) <= (Room p2 _) = p1 <= p2
-- Dungeon Generator (empty dungeon -> )

-- Empty Dungeon
--- Empty Cells
emptyCells :: Int -> Int -> Cells
emptyCells w h = listArray ((0,0),(w-1,h-1)) (concat (replicate (w*h) "W"))
--- Empty Dungeon
emptyDungeon :: Int -> Int -> Int -> Dungeon
emptyDungeon  w h s = (Dungeon (emptyCells w h) w h [] (mkStdGen s))
-- Dungeon
--- Generate new one
generateDungeon :: Int -> Int -> Int -> Dungeon
generateDungeon w h s = generatePassages (fillDungeon (emptyDungeon w h s))
--- (Re-)fill existing Dungeon
fillDungeon :: Dungeon -> Dungeon
fillDungeon (Dungeon c w h r g) = sortRooms d
    where d = splitRoomsX (Dungeon c w h r g) (Rectangle 0 0 (w-1) (h-1)) 0
--- Recursive split (generate random number, split)
splitRoomsX :: Dungeon -> Rectangle -> Int -> Dungeon
splitRoomsX (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) dep
    | isFinalRoom (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) dep = makeRoom (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) -- make room
    | otherwise = (splitRoomsY (splitRoomsY (Dungeon c w h r ng) top (dep+1)) down) (dep+1)
    where 
        (split, ng) = getSplit y1 y2 g -- random between y1 and y2
        top = (Rectangle x1 y1 x2 split)
        down = (Rectangle x1 (split+1) x2 y2)
splitRoomsY :: Dungeon -> Rectangle -> Int -> Dungeon
splitRoomsY (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) dep
    | isFinalRoom (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) dep = makeRoom (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) -- make room
    | otherwise = (splitRoomsX (splitRoomsX (Dungeon c w h r ng) left (dep+1)) right) (dep+1)
    where 
        (split, ng) = getSplit x1 x2 g -- random between x1 and x2
        left = (Rectangle x1 y1 split y2)
        right = (Rectangle (split+1) y1 x2 y2)
--- Generate Passages
generatePassages :: Dungeon -> Dungeon
generatePassages (Dungeon c w h r g) = connectNeighbours (selectSpanningTreePlus (Dungeon c w h r g) graph)
    where graph = buildG (0,(length r)-1) (getNeighbours (Dungeon c w h r g))
getNeighbours :: Dungeon -> [Edge]
getNeighbours (Dungeon c w h r g) = filter (\(x,y) -> x < y) (foldr (++) [] (map (getSurrounding (Dungeon c w h r g)) r))
--- Select spanning tree plus few randomly selected
selectSpanningTreePlus :: Dungeon -> Graph -> (Dungeon, Graph)
selectSpanningTreePlus (Dungeon c w h r g) graph = ((Dungeon c w h newRooms nng),newGraph)
    where
        treeE = treeToEdges ((dfs bigraph [ran]) !! 0)
        bigraph = buildG (0,(length r)-1) ((edges graph)++(edges (transposeG graph)))
        (ran,ng) = randomR (0,(length r)) g
        (randomE,nng) = selectRandomEdges (edges graph) ng
        randomEF = filter (\(a,b) -> (notElem (a,b) treeE) && (notElem (b,a) treeE)) randomE --Reduce duplicate edges
        newGraph = buildG (0,(length r)-1) (treeE++randomEF)
        newRooms = updateRooms r (topSort newGraph)
--- Update Rooms according to topological sort
updateRooms :: [Room] -> [Vertex] -> [Room]
updateRooms r v = addMeta (addMeta r (head v) "E") (last v) "B"
--- Add meta to nth room
addMeta :: [Room] -> Int -> String -> [Room]
addMeta ((Room p m):rs) 0 nm = ((Room p (m++nm)):rs)
addMeta (r:rs) c nm = r:(addMeta rs (c-1) nm)
--- Select random edges from list (could be generalized)
selectRandomEdges :: [Edge] -> StdGen -> ([Edge],StdGen)
selectRandomEdges [] g = ([],g)
selectRandomEdges (l:ls) g
    | ran == 0 = (l:selected,nng)
    | otherwise = (selected,nng)
    where 
        (ran,ng) = randomR (0 :: Int,4) g
        (selected,nng) = selectRandomEdges ls ng
--- Convert tree to graph
treeToEdges :: Tree Int -> [Edge]
treeToEdges (Node a s) = foldr (++) [] (map (\(Node b s2) -> [(a,b)]++(treeToEdges (Node b s2))) s)
--- Connect all neighbours from graph
connectNeighbours :: (Dungeon, Graph) -> Dungeon
connectNeighbours ((Dungeon c w h r g),graph) = foldr (connectNeighbour) (Dungeon c w h r g) (edges graph)
--- Connect edge (add passage to cells)
connectNeighbour :: Edge -> Dungeon -> Dungeon
connectNeighbour (u,v) (Dungeon c w h r g)
    | (vx1<=ux2)&&(vy1<=uy2) = connectNeighbour (v,u) (Dungeon c w h r g) --Ensure relative location
    | ux2 < vx1 = connectHoriz (Dungeon c w h r g) (Rectangle (ux2+1) (max uy1 vy1) (vx1-1) (min uy2 vy2))
    | uy2 < vy1 = connectVerti (Dungeon c w h r g) (Rectangle (max ux1 vx1) (uy2+1) (min ux2 vx2)(vy1-1) )
    | otherwise = trace ("Unexpected:"++show (Rectangle ux1 uy1 ux2 uy2)++" , "++show (Rectangle vx1 vy1 vx2 vy2)) (Dungeon c w h r g) --Unhandled exception
    where 
        (Room (Rectangle ux1 uy1 ux2 uy2) _) = r !! u
        (Room (Rectangle vx1 vy1 vx2 vy2) _) = r !! v
connectHoriz :: Dungeon -> Rectangle -> Dungeon
connectHoriz (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) = (Dungeon cn w h r ng)
    where
        (ran,ng) = randomR (y1,y2 :: Int) g
        cn = c // assocs (listArray ((x1,ran),(x2,ran)) (concat $ replicate (x2-x1+1) "P"))
connectVerti :: Dungeon -> Rectangle -> Dungeon
connectVerti (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) = (Dungeon cn w h r ng)
    where
        (ran,ng) = randomR (x1,x2 :: Int) g
        cn = c // assocs (listArray ((ran,y1),(ran,y2)) (concat $ replicate (y2-y1+1) "P"))
--- Get all surrounding Rooms
getSurrounding :: Dungeon -> Room -> [Edge]
getSurrounding (Dungeon c w h r g) room
    | index == -1 = trace "ERROR" [] --Unhandled exception
    | otherwise = map (\x -> (index,fromMaybe (-1) (elemIndex x r)))((surl room (Dungeon c w h r g))++(surr room (Dungeon c w h r g))++(suru room (Dungeon c w h r g))++(surd room (Dungeon c w h r g)))
    where
        index = fromMaybe (-1) (elemIndex room r)
-- Utility
--- Surrounding left/right/up/down
surl :: Room -> Dungeon -> [Room]
surl (Room (Rectangle x1 y1 x2 y2) _) d
    | x1 <= 1 = [] --OutOfBounds
    | otherwise = sortUniq (surl_r d (range ((x1-1,y1),(x1-1,y2))) 0)
surr :: Room -> Dungeon -> [Room]
surr (Room (Rectangle x1 y1 x2 y2) _) d
    | x2 >= ((width d)-2) = [] --OutOfBounds
    | otherwise = sortUniq (surr_r d (range ((x2+1,y1),(x2+1,y2))) 0)
suru :: Room -> Dungeon -> [Room]
suru (Room (Rectangle x1 y1 x2 y2) _) d
    | y1 <= 1 = [] --OutOfBounds
    | otherwise = sortUniq (suru_r d (range ((x1,y1-1),(x2,y1-1))) 0)
surd :: Room -> Dungeon -> [Room]
surd (Room (Rectangle x1 y1 x2 y2) _) d
    | y2 >= ((height d)-2) = [] --OutOfBounds
    | otherwise = sortUniq (surd_r d (range ((x1,y2+1),(x2,y2+1))) 0)
surl_r :: Dungeon -> [(Int,Int)] -> Int -> [Room]
surl_r d points dep
    | dep >= 5 = []
    | length found == 0 = surl_r d (map (\(x,y) -> (x-1,y)) points) (dep+1)
    | otherwise = found
    where found = foldr (++) [] (map (findRoom d) points)
surr_r :: Dungeon -> [(Int,Int)] -> Int -> [Room]
surr_r d points dep
    | dep >= 5 = []
    | length found == 0 = surr_r d (map (\(x,y) -> (x+1,y)) points) (dep+1)
    | otherwise = found
    where found = foldr (++) [] (map (findRoom d) points)
suru_r :: Dungeon -> [(Int,Int)] -> Int -> [Room]
suru_r d points dep
    | dep >= 5 = []
    | length found == 0 = suru_r d (map (\(x,y) -> (x,y-1)) points) (dep+1)
    | otherwise = found
    where found = foldr (++) [] (map (findRoom d) points)
surd_r :: Dungeon -> [(Int,Int)] -> Int -> [Room]
surd_r d points dep
    | dep >= 5 = []
    | length found == 0 = surd_r d (map (\(x,y) -> (x,y+1)) points) (dep+1)
    | otherwise = found
    where found = foldr (++) [] (map (findRoom d) points)
--- Find Room by point
findRoom :: Dungeon -> (Int,Int) -> [Room]
findRoom d (x,y) = (filter (\(Room (Rectangle x1 y1 x2 y2) _) -> x <= x2 && x >= x1 && y <= y2 && y >= y1) (rooms d))
--- Sort Rooms
sortRooms :: Dungeon -> Dungeon
sortRooms (Dungeon c w h r g) = (Dungeon c w h (sort r) g) 
--- Create a room
makeRoom :: Dungeon -> Rectangle -> Dungeon
makeRoom (Dungeon c w h r g) (Rectangle x1 y1 x2 y2)
    | ((x1+1)>(x2-1))||((y1+1)>(y2-1)) = (Dungeon c w h r g)
    | otherwise = (Dungeon cn w h ((Room (Rectangle (x1+1) (y1+1) (x2-1) (y2-1)) "") : r) g)
    where cn = c // assocs (listArray ((x1+1,y1+1),(x2-1,y2-1)) (concat $ replicate ((x2-x1-1)*(y2-y1-1)) "."))
--- Split string by n characters
splitN :: String -> Int -> String
splitN s n = unlines (chunksOf n s)
--- Get random number from x to y
getSplit :: Int -> Int -> StdGen -> (Int, StdGen)
getSplit x y g -- x < y
    | x > y = getSplit y x g
    | y-x > 6  = randomR (x+2,y-2) g
    | otherwise = (x + ((y-x) `div` 2), g)
--- Condition for recursion end
isFinalRoom :: Dungeon -> Rectangle -> Int -> Bool
isFinalRoom (Dungeon c w h r g) (Rectangle x1 y1 x2 y2) dep = (abs (x1 - x2) < 5) || (abs (y1 - y2) < 5) || (getRandomStop dep g)
--- Randomly stop after certain depth
getRandomStop :: Int -> StdGen -> Bool
getRandomStop d g
    | d < 3 = False
    | d > 6 = (fst (randomR (0,6) g)) == (0 :: Int)
    | otherwise = (fst (randomR (0,10) g)) == (0 :: Int)
-- Main, should read parameters
main :: IO ()
main = do
    args <- getArgs
    seed <- randomIO :: IO Int
    case length args of
        2 -> print (generateDungeon (read (args !! 1))  (read (args !! 0)) seed)
        3 -> print (generateDungeon (read (args !! 1))  (read (args !! 0)) (read (args !! 2)))
        otherwise -> putStrLn "Usage: <w> <h> [s]\n  Dungeon Generator:\n    1) layout of a dungeon ('W' - wall,'.'- room,'P' - passage)\n    2) list of rooms (<ID>: (Coords) <Meta>\n      where Meta is a string of characters meaning the rooms is Entrance/Boss room"