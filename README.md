# DUNGEON GENERATOR

#### Dungeon generator in haskell

-----------------

##### Features:
* Generating a random dungeon
* User-defined size/seed
* Generating metadata : Room location/size and entrance/boss room according to topological sort

##### Algorithm:
* Create a layout and a list of rooms using binary division
* For each room get all neighbours and create a graph
* Select a random node and using dfs take a spanning tree of the graph
* Add a few random edges from the original graph
* Add metadata to first/last rooms of topological sort of the new graph
